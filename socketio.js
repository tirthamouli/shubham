// Step 1: Require the modules
const socketIO = require('socket.io');

// Step 2: Export the default function that needs to be called
module.exports = (server) => {
  const io = socketIO(server, {
    log: false,
    agent: false,
    origins: '*:*',
    transports: [
      'websocket',
      'htmlfile',
      'xhr-polling',
      'jsonp-polling',
      'polling',
    ],
  });

  /**
   * When we have a new connection this event is triggered
   */
  io.on('connection', (socket) => {
    // eslint-disable-next-line no-console
    console.log('We have a new connection ', socket.id);

    /**
      * Message route
      */
    socket.on('message', (value) => {
      // socket.broadcast.emit('message', value);
      io.emit('message', value);
    });
  });
};
