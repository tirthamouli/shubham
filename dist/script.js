/* eslint-disable no-undef */

// Step 1: Connect to the socket
const socket = io('/', {
  reconnection: true,
  reconnectionDelay: 1000,
  reconnectionDelayMax: 5000,
  reconnectionAttempts: 99999,
});

// Step 2: Socket listeners
socket.on('connect', () => {
  // eslint-disable-next-line no-console
  console.log('Socket connected');
});

socket.on('message', ({ username, message }) => {
  const li = document.createElement('li');
  li.innerHTML = `<span class='username'>${username}</span> 
   <span class ='message'>${message}</span>`;
  document.getElementById('chat-area').appendChild(li);
});

// Normal event handlers
document.getElementById('send').addEventListener('click', () => {
  const username = document.getElementById('username').value;
  const message = document.getElementById('message').value;
  socket.emit('message', {
    username,
    message,
  });
  document.getElementById('message').value = '';
});
