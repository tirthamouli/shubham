// Step 1: Requiring modules
const express = require('express');
const http = require('http');
// Custom
const socket = require('./socketio');

// Step 2: Create a new express application
const app = express();

// Setting up static path
app.use(express.static('dist'));

// Step 3: Route listeners
app.get('/hello', (req, res) => {
  res.status(200).json({
    hello: 'world',
    foo: 'bar',
  });
});

// Step 4: Create a server object
const server = http.Server(app);

// Step 5: Socket handling
socket(server);

// Step 6: Listening on a port
server.listen(3000, () => {
  // eslint-disable-next-line no-console
  console.log('Listening on port 3000');
});
